image: gradle:8.0.1-jdk17

stages:
  - build
  - test
  - package
  - publish
  - deploy

before_script:
  - export APP_VERSION="${APP_VERSION:-${CI_COMMIT_TAG#release-}}"
  - export APP_VERSION="${APP_VERSION:-snapshot.${CI_COMMIT_SHORT_SHA}.${CI_PIPELINE_ID}}"
  - export APP_VERSION_PATCH="${APP_VERSION}"
  - export APP_VERSION_MINOR="${APP_VERSION%.*}"
  - export APP_VERSION_MAJOR="${APP_VERSION%.*.*}"

build-job:
  stage: build
  script:
    - echo "Building the ${APP_VERSION} version"
    - gradle bootJar --no-daemon
  artifacts:
    expire_in: 1 day
    paths:
      - build/libs/*.jar

test-job:
  stage: test
  script:
    - gradle test --no-daemon
  needs:
    - build-job
  dependencies:
    - build-job

package-job:
  stage: package
  image: docker:stable
  services:
    - docker:dind
  script:
    - echo "${CI_REGISTRY_PASSWORD}" | docker login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"
    - docker build --cache-from "${CI_REGISTRY_IMAGE}":latest --pull -t "${CI_REGISTRY_IMAGE}" --build-arg "APP_VERSION=${APP_VERSION}" -f ./Dockerfile .
    - docker tag "${CI_REGISTRY_IMAGE}" "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
    - docker push "${CI_REGISTRY_IMAGE}"
    - docker logout "${CI_REGISTRY}"
  needs:
    - build-job
  dependencies:
    - build-job

publish-job:
  stage: publish
  image: docker:stable
  services:
    - docker:dind
  rules:
    - if: $CI_COMMIT_TAG =~ /^release-/
  script:
    - echo "${CI_REGISTRY_PASSWORD}" | docker login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"
    - docker pull "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
    - |-
      for image_tag in "${APP_VERSION}" "${APP_VERSION_PATCH}" "${APP_VERSION_MINOR}" "${APP_VERSION_MAJOR}"
      do
        docker tag "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" "${CI_REGISTRY_IMAGE}:${image_tag}"
        docker push "${CI_REGISTRY_IMAGE}:${image_tag}"
      done
  needs:
    - test-job
    - package-job

deploy-job:
  stage: deploy
  image: alpine:latest
  rules:
    - if: >-
        $KUBERNETES_CA_CERT &&
        $KUBERNETES_URL &&
        $KUBERNETES_NAMESPACE &&
        $KUBERNETES_TOKEN &&
        $CI_COMMIT_TAG =~ /^release-/
      when: manual
      allow_failure: true
  script:
    - tempdir="$(mktemp -d)"
    - apk add --no-cache curl
    - curl -L -s -o "${tempdir}/kubectl" https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    - install -m 0755 "${tempdir}/kubectl" /usr/local/bin/kubectl
    - rm -rf "${tempdir}"
    - kubectl config set-cluster k8s --server="${KUBERNETES_URL}"
    - kubectl config set-credentials admin --token="${KUBERNETES_TOKEN}"
    - kubectl config set-context default --cluster=k8s --user=admin
    - kubectl config set-context default --namespace="${KUBERNETES_NAMESPACE}"
    - kubectl config use-context default
    - kubectl config set clusters.k8s.certificate-authority-data "${KUBERNETES_CA_CERT}"
    - sed -i -r "s#^([[:space:]]+image:) .+\$#\1 ${CI_REGISTRY_IMAGE}:${APP_VERSION}#" ./k8s/pod.yaml
    - kubectl apply -f ./k8s/pod.yaml
  needs:
    - publish-job

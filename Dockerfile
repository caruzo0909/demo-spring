FROM openjdk:17-alpine
ARG APP_VERSION
ENV APP_VERSION=${APP_VERSION:-snapshot}
ENV JAR_SOURCE=build/libs/spring-boot-${APP_VERSION}.jar
VOLUME /tmp
ADD $JAR_SOURCE application.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/application.jar"]
